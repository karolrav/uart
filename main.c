#include <stm32l476xx.h>
#include <stm32l4xx_hal.h>
#include <string.h>
#include <stdio.h> 
#include <stdlib.h>
//#include <stm32_hal_legacy.h>

void func(double);
void SysTick_Handler(void)
{
    HAL_IncTick();
    HAL_SYSTICK_IRQHandler();
}
static UART_HandleTypeDef s_UARTHandle;
int main(void)
{
 RCC->AHB2ENR    |=0x1;  
 RCC->APB1ENR1 |=0x1; // ->TIM2EN;
 RCC->APB1ENR1  |=0x20000; // USART2EN enalbe usart clock
//
HAL_Init();
    __USART2_CLK_ENABLE();
    __GPIOA_CLK_ENABLE();
    
    GPIO_InitTypeDef GPIO_InitStructure;
 
//

int Tim1Prescaler= (uint16_t) (SystemCoreClock/1) - 1; 
int Period = 4;
//gipio config
GPIOA->MODER &= ~(0x800); //set first bit
 GPIOA->OTYPER   &= 0x0C000000; // 00
 GPIOA->OSPEEDR  ^= 0xFFFFFFFF; //11
 GPIOA->PUPDR    &= 0x64000000; //00
GPIOA->ODR    =0x0020;

 
 //GPIOA->MODER &=0xfc; 
 
//timer config
TIM2->SR   |=0x2;
TIM2->PSC =Tim1Prescaler;
TIM2->ARR = Period; // 0 - ARR then cnt is set to 0 again //periodas 55s
TIM2->DIER =TIM_DIER_UIE; // set the update interrupt
TIM2->CR1 |=TIM_CR1_CEN;//timer options, with this register we need to start the timer
NVIC_EnableIRQ(TIM2_IRQn); // Enable interrupt from TIM2 (NVIC level)

//***********************************
 /* Connect PD5 to USART1_Tx */ //PA12/11
 GPIO_InitStructure.Pin = GPIO_PIN_2;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Alternate = GPIO_AF7_USART2;
    GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_InitStructure.Pin = GPIO_PIN_3;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_OD;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

//*********************************
    s_UARTHandle.Instance        = USART2;
    s_UARTHandle.Init.BaudRate   = 115200;
    s_UARTHandle.Init.WordLength = UART_WORDLENGTH_8B;
    s_UARTHandle.Init.StopBits   = UART_STOPBITS_1;
    s_UARTHandle.Init.Parity     = UART_PARITY_NONE;
    s_UARTHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    s_UARTHandle.Init.Mode       = UART_MODE_TX_RX;
 if (HAL_UART_Init(&s_UARTHandle) != HAL_OK )
        asm("bkpt 255");

         for (;;)
    {
        double a;
         char buffer[]="";
    //  if(buffer!=0x0D0A0D0A){
        HAL_UART_Receive(&s_UARTHandle, (uint8_t *)buffer, sizeof(buffer), HAL_MAX_DELAY);
       a=atoi(buffer);
      // a= buffer>>16;
       //memcpy(&a,&buffer, sizeof(double));
       if(a!=0){
       func(a);   
       }        //A=0;
        HAL_UART_Transmit(&s_UARTHandle, (uint8_t *)buffer, sizeof(buffer), HAL_MAX_DELAY);
       
    }
    
 
}
void func(double b)
{ 
 //RCC->APB1ENR1 |=0x1; // ->TIM2EN;

  int Tim1Prescaler1= (uint16_t) (SystemCoreClock/b) - 1; 
  int Period1 = 4;
   TIM2->SR   |=0x2;
TIM2->PSC =Tim1Prescaler1;
TIM2->ARR = Period1; // 0 - ARR then cnt is set to 0 again //periodas 55s 0000110100001010
TIM2->DIER =TIM_DIER_UIE; // set the update interrupt
TIM2->CR1 |=TIM_CR1_CEN;//ti
NVIC_EnableIRQ(TIM2_IRQn); 

} 

 void TIM2_IRQHandler(void)
  {
if(TIM2->SR & TIM_SR_UIF){
  // GPIOA->ODR  |= 1 << 1;
  //GPIOA->ODR &= ~(1 << 1);
   TIM2->SR &= ~TIM_SR_UIF;
 GPIOA->ODR  ^= 1 << 5; // 5
}
}

 